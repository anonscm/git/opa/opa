/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.profile;

import javax.portlet.PortletRequest;

/**
 * Interface that should be implemented for supported portal containers to retrieve user portal profile.
 * 
 * @author Mohamed Ghazouani
 */
public interface UserServiceUtil {

    /**
     * Find User Portal by Id.
     * 
     * @param id
     *            : id of user portal.
     * @return {@link User}
     */
    User getUserProfile(String id);

    /**
     * Find user Portal from PortletRequest.
     * 
     * @param request
     *            : PortletRequest
     * @return {@link User}
     */
    User getUserProfile(PortletRequest request);

}
