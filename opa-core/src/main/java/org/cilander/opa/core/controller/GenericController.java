/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.core.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cilander.opa.core.annotation.UsingOpenPortalAdapterService;
import org.cilander.opa.core.constants.OpaConstants;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.stereotype.Controller;
import org.springframework.web.portlet.mvc.AbstractController;

import java.lang.annotation.Annotation;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * GenericController class is intended to be subclassed.
 * 
 * @author oschmitz
 */
@Controller(value = OpaConstants.GENERIC_CONTROLLER)
public class GenericController extends AbstractController {

    protected final static Log LOGGER = LogFactory.getLog(GenericController.class);

    private Map<String, Object> services = new HashMap<String, Object>();

    /**
     * init the service via annotations.
     * 
     * @param clazz
     */
    @SuppressWarnings("unchecked")
    protected final void initService(final Class clazz) {

        final Annotation[] methodAnnotations = clazz.getAnnotations();
        for (Annotation annotation : methodAnnotations) {
            if (annotation instanceof UsingOpenPortalAdapterService) {
                final UsingOpenPortalAdapterService myAnnotation = (UsingOpenPortalAdapterService) annotation;
                for (final String serviceName : myAnnotation.serviceNames()) {
                    try {
                        this.services.put(serviceName, getApplicationContext().getBean(serviceName));
                    } catch (final NoSuchBeanDefinitionException nsbde) {
                        GenericController.LOGGER.warn(MessageFormat.format("Bean not found for service {0}", serviceName));
                        throw nsbde;
                    }
                }
            }
        }

    }

    /**
     * Method for getting an initialized service.
     * 
     * @param serviceName
     *            service to be looked for.
     * @return the service if the map contains it else it will return null.
     */
    public Object getService(final String serviceName) {
        return this.services.get(serviceName);
    }

}
