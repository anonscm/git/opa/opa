/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cilander.opa.core.constants.OpaConstants;

import java.text.MessageFormat;

/**
 * Util Class to identify OPA environment
 * 
 * @author Mohamed Ghazouani
 */
public class PortalContainerUtil {

    private static Log logger = LogFactory.getLog(PortalContainerUtil.class);

    /**
     * @return true if running under Liferay Portal Container
     */
    public static boolean isLiferayPortalContainer() {
        return PortalContainerUtil.canLoadClass(OpaConstants.LIFERAY_CONTAINER_DETECTION_CLASS_NAME);

    }

    /**
     * @return true if running under Jboss Portal Container
     */
    public static boolean isJbossPortalContainer() {
        return PortalContainerUtil.canLoadClass(OpaConstants.JBOSS_CONTAINER_DETECTION_CLASS_NAME);
    }

    public static boolean canLoadClass(final String className) {
        Class loaded = null;
        try {
            loaded = Class.forName(className);
        } catch (final ClassNotFoundException e) {
            PortalContainerUtil.logger.warn(MessageFormat.format("PortalContainerUtil.canLoadClass could not find {0}", className));
        }
        return loaded != null;
    }

}
