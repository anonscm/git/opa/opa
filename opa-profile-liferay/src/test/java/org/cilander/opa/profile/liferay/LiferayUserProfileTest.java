/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.profile.liferay;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.service.UserLocalServiceUtil;

import org.cilander.opa.profile.exception.UserNotFoundException;
import org.easymock.classextension.EasyMock;
import org.springframework.mock.web.portlet.MockRenderRequest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import junit.framework.Assert;

public class LiferayUserProfileTest {

    MockRenderRequest request;
    com.liferay.portal.model.User mockliferayUser;
    UserLocalServiceUtil mockUserLocalServiceUtil;
    LiferayUserServiceUtil liferayUserService;

    @BeforeTest
    public void setUp() {
        this.request = new MockRenderRequest();
        this.mockliferayUser = EasyMock.createMock(com.liferay.portal.model.User.class);
        this.mockliferayUser.setUserId(1);
        try {
            this.mockUserLocalServiceUtil = EasyMock.createMock(UserLocalServiceUtil.class);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        this.liferayUserService = new LiferayUserServiceUtil();
    }

    @Test
    public void getUserProfileTest() throws UserNotFoundException {

        this.request.setRemoteUser(null);
        Assert.assertNull(this.liferayUserService.getUserProfile(this.request));

    }

    @Test
    public void invalidProfileTest() {
        org.easymock.EasyMock.expect(Long.valueOf(this.mockliferayUser.getUserId())).andReturn(Long.valueOf(1)); //

        try {
            org.easymock.EasyMock.expect(this.mockliferayUser.getLogin()).andReturn("Test");
        } catch (PortalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        EasyMock.replay(this.mockliferayUser);
        // Assert.assertNotNull(liferayUserService.getUserFromLiferayProfile(mockliferayUser));

    }
}
