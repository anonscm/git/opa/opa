/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.profile.util;

import org.testng.Assert;
import org.testng.annotations.Test;

public class P3PConstantsTest {

    @Test
    public void getUserName() {
        new P3PConstants();
        Assert.assertNotNull(P3PConstants.INFO_USER_BDATE);
        Assert.assertNotNull(P3PConstants.INFO_USER_BUSINESS_INFO_ONLINE_EMAIL);
        Assert.assertNotNull(P3PConstants.INFO_USER_BUSINESS_INFO_ONLINE_URI);
        Assert.assertNotNull(P3PConstants.INFO_USER_BUSINESS_INFO_POSTAL_CITY);
        Assert.assertNotNull(P3PConstants.INFO_USER_BUSINESS_INFO_POSTAL_COUNTRY);
        Assert.assertNotNull(P3PConstants.INFO_USER_BUSINESS_INFO_POSTAL_ORGANIZATION);
        Assert.assertNotNull(P3PConstants.INFO_USER_BUSINESS_INFO_POSTAL_POSTALCODE);
        Assert.assertNotNull(P3PConstants.INFO_USER_BUSINESS_INFO_POSTAL_STATEPROV);
        Assert.assertNotNull(P3PConstants.INFO_USER_BUSINESS_INFO_POSTAL_STREET);
        Assert.assertNotNull(P3PConstants.INFO_USER_BUSINESS_INFO_TELECOM_FAX_COMMENT);
        Assert.assertNotNull(P3PConstants.INFO_USER_BUSINESS_INFO_TELECOM_MOBILE_COMMENT);
    }
}
