/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.profile.jboss;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cilander.opa.core.constants.OpaConstants;
import org.cilander.opa.profile.User;
import org.cilander.opa.profile.UserServiceUtil;
import org.cilander.opa.profile.impl.UserProfile;
import org.jboss.portal.identity.IdentityException;
import org.jboss.portal.identity.NoSuchUserException;
import org.jboss.portal.identity.UserModule;
import org.jboss.portal.identity.UserProfileModule;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.PortletRequest;

@Component(value = OpaConstants.USER_SERVICE_UTIL_NAME)
public class JbossUserServiceUtil implements UserServiceUtil {

    private static final Log log = LogFactory.getLog(JbossUserServiceUtil.class);

    public User getUserProfile(final String username) {
        User user = null;
        try {

            final UserModule userModule = (UserModule) new InitialContext().lookup(JbossConstants.USER_MODULE_JNDI_URL);
            final UserProfileModule jbossUserProfileModule = (UserProfileModule) new InitialContext().lookup(JbossConstants.USER_PROFILE_MODULE_JNDI_URL);
            final org.jboss.portal.identity.User jbossUser = userModule.findUserByUserName(username);
            final Map<String, String> props = jbossUserProfileModule.getProperties(jbossUser);
            user = createUserFromJbossProfile(jbossUser, props);
        } catch (NamingException e) {
            JbossUserServiceUtil.log.error("Could not locate Jboss UserModule");
        } catch (IllegalArgumentException e) {
            JbossUserServiceUtil.log.error(MessageFormat.format("invalid User username:{0}", username));
        } catch (NoSuchUserException e) {
            JbossUserServiceUtil.log.error(e.getMessage());
        } catch (IdentityException e) {
            JbossUserServiceUtil.log.error(e.getMessage());
        }
        return user;

    }

    public User getUserProfile(final PortletRequest request) {
        return getUserProfile(request.getRemoteUser());
    }

    User createUserFromJbossProfile(final org.jboss.portal.identity.User jbossUser, final Map props) {
        final UserProfile user = new UserProfile();
        if (jbossUser.getUserName() != null) {
            user.setLoginName(jbossUser.getUserName());
        }
        if (jbossUser.getId() != null) {
            user.setUserId(String.valueOf(jbossUser.getId()));
        }

        if (props.get(org.jboss.portal.identity.User.INFO_USER_EMAIL_REAL) != null) {
            user.setEmail((String) props.get(org.jboss.portal.identity.User.INFO_USER_EMAIL_REAL));
        }
        if (props.get(org.jboss.portal.identity.User.INFO_USER_NAME_GIVEN) != null) {
            user.setFirstName((String) props.get(org.jboss.portal.identity.User.INFO_USER_NAME_GIVEN));
        }
        if (props.get(org.jboss.portal.identity.User.INFO_USER_NAME_FAMILY) != null) {
            user.setLastName((String) props.get(org.jboss.portal.identity.User.INFO_USER_NAME_FAMILY));
        }

        return user;
    }

}
