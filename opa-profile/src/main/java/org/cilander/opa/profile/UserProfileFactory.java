/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.profile;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cilander.opa.profile.impl.UserProfile;
import org.cilander.opa.util.PortalContainerUtil;
import org.springframework.stereotype.Component;

import javax.portlet.PortletRequest;

/**
 * Factory class to retrieve UserProfile.
 * 
 * @author Mohamed Ghazouani
 */

@Component
public class UserProfileFactory {

    private static final Log LOG = LogFactory.getLog(UserProfileFactory.class);

    protected static UserServiceUtil serviceUtil;

    protected static GenericUserProfile genericUserProfile;

    public UserProfileFactory() {
        try {
            if (PortalContainerUtil.isJbossPortalContainer()) {
                initialServiceUtil("org.cilander.opa.profile.jboss.JbossUserServiceUtil");
            } else if (PortalContainerUtil.isLiferayPortalContainer()) {
                initialServiceUtil("org.cilander.opa.profile.liferay.LiferayUserServiceUtil");
            }
            final Class<?> userProfile = Class.forName("org.cilander.opa.profile.impl.GenericUserProfileImpl");
            UserProfileFactory.genericUserProfile = (GenericUserProfile) userProfile.newInstance();
        } catch (Exception e) {
            UserProfileFactory.LOG.warn(e.getLocalizedMessage());
        }

    }

    public void initialServiceUtil(final String className) throws ClassNotFoundException, Exception {
        try {
            final Class<UserServiceUtil> userServiceUtil = (Class<UserServiceUtil>) Class.forName(className);
            UserProfileFactory.serviceUtil = userServiceUtil.newInstance();
        } catch (ClassNotFoundException e) {
            UserProfileFactory.LOG.warn(e.getLocalizedMessage());
            throw e;
        } catch (Exception e) {
            UserProfileFactory.LOG.warn(e.getLocalizedMessage());
            throw e;
        }
    }

    /**
     * A Factory Method that retrieve User Profile based on User Id.
     * 
     * @param id
     *            : user Id
     * @return {@link User}
     */

    public static User getUserProfile(final String id) {
        if (UserProfileFactory.serviceUtil == null) {
            final StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Portal Container not identified, use UserProfileFactory.getUserProfile(PortletRequest request) instead,");
            stringBuilder.append(" to retrieve user attributes stored in PortletRequest");
            UserProfileFactory.LOG.warn(stringBuilder.toString());
            return new UserProfile();
        }
        final User userProfile = UserProfileFactory.serviceUtil.getUserProfile(id);
        return userProfile == null ? new UserProfile() : userProfile;
    }

    /**
     * A Factory Method that retrieve User Profile based on PortletRequest.
     * 
     * @param request
     *            {@link PortletRequest}
     * @return {@link User}
     */

    public static User getUserProfile(final PortletRequest request) {
        if (UserProfileFactory.serviceUtil == null) {
            return UserProfileFactory.genericUserProfile.getGenericUser(request);
        }
        return UserProfileFactory.getUserProfile(request.getRemoteUser());
    }

}
