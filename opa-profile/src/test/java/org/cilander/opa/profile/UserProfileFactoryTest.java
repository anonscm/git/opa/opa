/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.profile;

import org.cilander.opa.profile.util.P3PConstants;
import org.cilander.opa.util.PortalContainerUtil;
import org.springframework.mock.web.portlet.MockRenderRequest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class UserProfileFactoryTest extends UserProfileFactory {

    private MockRenderRequest request;
    private UserProfileFactory factory;
    private PortalContainerUtil mockPortalConatinerUtil;

    @BeforeTest
    public void setUp() {
        this.request = new MockRenderRequest();
        this.request.setRemoteUser("TEST");
        this.request.setAttribute(P3PConstants.INFO_USER_NAME_FAMILY, "lastName");
        this.request.setAttribute(P3PConstants.INFO_USER_NAME_NICKNAME, "user");
        this.factory = new UserProfileFactory();
    }

    @AfterTest
    public void tearDown() {
        this.request = null;
        this.factory = null;
    }

    @Test
    public void getUserByIdTest() {
        final User user = UserProfileFactory.getUserProfile("id");
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLastName(), "");
    }

    @Test(expectedExceptions = { ClassNotFoundException.class, InstantiationException.class, IllegalAccessException.class })
    public void initializeServiceTest() throws Exception {
        this.factory.initialServiceUtil("Huhu");
    }

    @Test
    public void serviceUtilNotSetTest() {
        this.factory = new UserProfileFactory();
        UserProfileFactory.serviceUtil = null;
        final User user = UserProfileFactory.getUserProfile("id");
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLastName(), "");
    }

    @Test
    public void getGuestUserServiceUtilNotSet() {
        this.factory = new UserProfileFactory();
        UserProfileFactory.serviceUtil = null;
        final User user = UserProfileFactory.getUserProfile(this.request);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLastName(), "");
    }

    @Test
    public void getGuestUser() {
        this.factory = new UserProfileFactory();
        final User user = UserProfileFactory.getUserProfile(this.request);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLastName(), "");
    }

    @Test
    public void getGuest2User() {
        System.setProperty("failProfile", "hallo");
        this.factory = new UserProfileFactory();
        UserProfileFactory.genericUserProfile = null;
        final User user = UserProfileFactory.getUserProfile("id");
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLastName(), "");
    }
}
