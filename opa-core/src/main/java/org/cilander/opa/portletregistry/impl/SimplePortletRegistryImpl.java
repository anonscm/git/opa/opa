package org.cilander.opa.portletregistry.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.cilander.opa.portletregistry.PortletRegistry;

/**
 * This really simple registry is just build for research purposes.
 * It uses a map to save the portlet registrations.
 *
 * Parameters are simply appended. This won't work on many portlet containers!
 * @author pcorne
 * 
 */
public class SimplePortletRegistryImpl implements PortletRegistry {

  Map<String, SimpleRegistryItem> items = new HashMap<String, SimpleRegistryItem>();

  /**
   * {@inheritDoc }
   * @param portletId
   * @param baseUrl
   * @param serviceName
   */
  public void registerPortlet(String portletId, String baseUrl, String serviceName) {
    assert serviceName != null;
    //Use portletId as key because servicenames may appear multiple times
    items.put(portletId, new SimpleRegistryItem(portletId, baseUrl, serviceName));
  }

  /**
   * {@inheritDoc }
   *
   * @param serviceName
   * @return
   */
  public String getEncodedUrl(String serviceName) {
    SimpleRegistryItem item = getItemByServiceName(serviceName);
    if (item == null) {
      return null;
    } else {
      return item.getBaseUrl();
    }
  }

  /**
   * {@inheritDoc }
   *
   * @param portletId
   */
  public void unregisterPortlet(String portletId) {
    items.remove(portletId);
  }

  /**
   * {@inheritDoc }
   *
   * @param serviceName
   * @param params
   * @return
   */
  public String getEncodedUrl(String serviceName, Map<String, String> params) {
    String baseUrl = getEncodedUrl(serviceName);
    StringBuilder result = new StringBuilder(baseUrl);
    boolean firstParameter = false;
    //Append when no parameter start char has been found
    if (!baseUrl.contains("?")) {
      result.append("?");
      firstParameter=true;
    }
    Set<String> keys = params.keySet();
    for (String key : keys) {
      try {
        String value = params.get(key);
        if (!firstParameter) {
          result.append("&");
        } else {
          firstParameter = false;
        }
        result.append(URLEncoder.encode(key, "ISO-8859-15"));
        result.append("=");
        result.append(URLEncoder.encode(value, "ISO-8859-15"));
      } catch (UnsupportedEncodingException ex) {
        Logger.getLogger(SimplePortletRegistryImpl.class.getName()).log(Level.SEVERE, "Encoding problem", ex);
      }
    }
    return result.toString();
  }

  /**
   * Searches the registry item for the given servicename
   * @param serviceName
   * @return
   */
  private SimpleRegistryItem getItemByServiceName(String serviceName) {
    SimpleRegistryItem item = null;
    Iterator<String> it = items.keySet().iterator();
    while (it.hasNext()) {
      item = items.get(it.next());
      if (item != null && item.getServiceName().equals(serviceName)) {
        return item;
      }
    }
    return null;
  }
}
