package org.cilander.opa.portletregistry;

import java.util.Map;

/**
 * This interface describes the functionality that may be used for portlets to
 * resolve a symbolic name to a generated url that can be used to call portlet
 * url.
 *
 * The implementation of the url generation depends on the portlet container.
 * @author pcorne
 */
public interface PortletRegistry {

  /**
   * This method registers a portlet with the given portlet id and servicename
   * and stores the portlet and the additionally given baseUrl in the registry.
   *
   * NOT IMPLEMENTED YET:<br/>
   * The servicename will be used later to fetch the url. When two portlets with
   * the same servicename are registered, then the first hit in the registry
   * will be used. (May result in "weird" results!)
   *
   * To solve the multiple servicename problem, the assign method may be used to
   * bind exactly one portlet to a servicename. <br/>
   * (for example via some kind of admin interface)
   * @param portletId
   * @param baseUrl
   * @param serviceName
   */
  void registerPortlet(String portletId, String baseUrl, String serviceName);

  /**
   * Removes the portlet and it's assignments from the registry. 
   * 
   * NOT IMPLEMENTED YET:<br/>
   * When the given portlet was assigned to a servicename by hand, then the
   * first hit will be used again, if there are still multiple portlets for the
   * same servicename available.
   * @param portletId
   */
  void unregisterPortlet(String portletId);

  /**
   * Returns a simple url that calls the portlet that is registered for the
   * given servicename. 
   * 
   * NOT IMPLEMENTED YET:<br/>
   * When multiple portlets are registeres and no dedicated
   * assignment has been configured then this method will return the first found
   * portlet with the given servicename.
   * @param serviceName
   * @return
   */
  String getEncodedUrl(String serviceName);

  /**
   * This method does the same as {@link #getEncodedUrl(java.lang.String) }
   * except that it includes the given parameters in the url.
   *
   * Unsafe characters in the parameters will be encoded.
   * @param serviceName
   * @param params
   * @return
   */
  String getEncodedUrl(String serviceName, Map<String, String> params);
  /**NOT IMPLEMENTED YET:<br/>
   * This method assigns a given portletId with the given servicename.
   *
   * The portletId needs to be registered prior to this call. The "regular"
   * servicename of the portlet remains untouched and the portlet may be found
   * via all servicenames that are assigned to it. Except when the regular
   * servicename has been used in an assignement. (Because they always are
   * prefered)
   * @param portletId
   * @param serviceName
   */
  //void assign(String portletId, String serviceName);
  /**NOT IMPLEMENTED YET:<br/>
   * removes the assignment of the given serviceName to the given portletID.
   * @param portletId
   * @param serviceName
   */
  //void removeAssignment(String portletId, String serviceName);
}
