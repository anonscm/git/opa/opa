/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.profile;

import java.util.Date;

/**
 * An Object Model that hold user profile.
 * 
 * @author Mohamed Ghazouani
 */
public interface User {

    /**
     * User Gender.
     */
    public enum Gender {
        /**
         * For Male.
         */
        MALE,
        /**
         * For Female.
         */
        FEMALE
    }

    /**
     * Method for getting user first name.
     * 
     * @return first name of the user is returned if set else empty String.
     */
    String getFirstName();

    /**
     * Method for setting user first name.
     * 
     * @param firstName
     */
    void setFirstName(String firstName);

    /**
     * Method for getting user last name.
     * 
     * @return last name of the user is returned if set else empty String.
     */
    String getLastName();

    /**
     * Method for setting user last name.
     * 
     * @param lastName
     */
    void setLastName(String lastName);

    /**
     * Method for getting user E-mail Address.
     * 
     * @return E-mail Address of the user is returned if set else empty String
     */
    String getEmail();

    /**
     * Method for setting user E-mail Address.
     * 
     * @param email
     */
    void setEmail(String email);

    /**
     * Method for getting user login/nickname.
     * 
     * @return login/nickname of the user is returned if set else empty String.
     */
    String getLoginName();

    /**
     * Method for setting user login/nickname.
     * 
     * @param loginName
     */
    void setLoginName(String loginName);

    /**
     * Method for setting user Id.
     * 
     * @param userId
     */
    void setUserId(String userId);

    /**
     * Method for getting user Id.
     * 
     * @return user Id is returned if set else empty String.
     */
    String getUserId();

    /**
     * Method for getting user gender
     * 
     * @return {@link Gender}
     */
    Gender getGender();

    /**
     * Method for setting user {@link Gender}.
     * 
     * @param {@link Gender}
     */
    void setGender(Gender gender);

    /**
     * Method for getting user birth date.
     * 
     * @return user birth date if set else null.
     */
    Date getBirthDate();

    /**
     * Method for setting user birth date.
     * 
     * @param birthDate
     *            .
     */
    void setBirthDate(Date birthDate);

}
