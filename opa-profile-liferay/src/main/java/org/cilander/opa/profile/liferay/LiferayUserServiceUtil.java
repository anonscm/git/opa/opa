/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.profile.liferay;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.exception.NestableException;
import com.liferay.portal.service.UserLocalServiceUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cilander.opa.core.constants.OpaConstants;
import org.cilander.opa.profile.User;
import org.cilander.opa.profile.UserServiceUtil;
import org.cilander.opa.profile.impl.UserProfile;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

import javax.portlet.PortletRequest;

@Component(value = OpaConstants.USER_SERVICE_UTIL_NAME)
public class LiferayUserServiceUtil implements UserServiceUtil {

    private static final Log log = LogFactory.getLog(LiferayUserServiceUtil.class);

    public User getUserProfile(final String id) {
        User user = null;
        if (id == null) {
            // the user seems not to be logged in
            return user;
        }
        com.liferay.portal.model.User liferayUser = null;
        try {
            liferayUser = UserLocalServiceUtil.getUser(Long.parseLong(id));
        } catch (NestableException e) {
            LiferayUserServiceUtil.log.error(e);
        }
        if (liferayUser == null) {
            LiferayUserServiceUtil.log.warn(MessageFormat.format("User with id {0} could not be found!", id));
        } else {
            user = getUserFromLiferayProfile(liferayUser);
        }
        return user;

    }

    public User getUserProfile(final PortletRequest request) {

        return getUserProfile(request.getRemoteUser());
    }

    private User getUserFromLiferayProfile(final com.liferay.portal.model.User liferayUser) {
        UserProfile user = new UserProfile();

        try {
            if (liferayUser.getLogin() != null) {
                user.setLoginName(liferayUser.getLogin());
            }
        } catch (PortalException e) {
            LiferayUserServiceUtil.log.error(e);
        } catch (SystemException e) {
            LiferayUserServiceUtil.log.error(e);
        }
        if (liferayUser.getUserId() != -1) {
            user.setUserId(String.valueOf(liferayUser.getUserId()));
        }
        if (liferayUser.getFirstName() != null) {
            user.setFirstName(liferayUser.getFirstName());
        }

        if (liferayUser.getLastName() != null) {
            user.setLastName(liferayUser.getLastName());
        }

        if (liferayUser.getEmailAddress() != null) {
            user.setEmail(liferayUser.getEmailAddress());
        }
        if (liferayUser.isMale()) {
            user.setGender(User.Gender.MALE);
        } else {
            user.setGender(User.Gender.FEMALE);
        }
        if (liferayUser.getBirthday() != null) {
            user.setBirthDate(liferayUser.getBirthday());
        }

        return user;
    }
}
