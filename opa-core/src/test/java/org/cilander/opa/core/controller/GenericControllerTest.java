/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.core.controller;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import org.cilander.core.annotations.MyAnnotatedClass;
import org.cilander.opa.core.annotation.UsingOpenPortalAdapterService;
import org.cilander.opa.core.constants.OpaConstants;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GenericControllerTest {

    @Test
    public void getAnnotatedValue() {
        boolean failed = false;
        final Class aClass = GenericController.class;

        final Field[] declaredFields = aClass.getDeclaredFields();
        for (final Field field : declaredFields) {
            final Annotation[] methodAnnotations = field.getAnnotations();
            for (Annotation annotation : methodAnnotations) {
                if (annotation instanceof UsingOpenPortalAdapterService) {
                    try {
                        final UsingOpenPortalAdapterService myAnnotation = (UsingOpenPortalAdapterService) annotation;
                        Assert.assertEquals(myAnnotation.serviceNames()[0], OpaConstants.GENERIC_CONTROLLER);

                        final Object newInstance = aClass.newInstance();

                        Assert.assertTrue(((GenericController) newInstance).getApplicationContext().containsBean(myAnnotation.serviceNames()[0]));

                        final Object invoked = field.get(newInstance);
                        Assert.assertTrue(invoked instanceof Object);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                        failed = true;
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                        failed = true;
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                        failed = true;
                    }
                }
            }
            Assert.assertFalse(failed);
        }

    }

    @Test
    public void getContext() {
        final GenericController controller = new MyAnnotatedClass();
        final ApplicationContext context = controller.getApplicationContext();
        Assert.assertNotNull(context);
        Assert.assertTrue(context instanceof ClassPathXmlApplicationContext);
    }

    @Test
    public void getService() {
        final GenericController controller = new MyAnnotatedClass();
        final Object service = controller.getService(OpaConstants.GENERIC_CONTROLLER);
        Assert.assertNull(service);
    }

    @Test
    public void initService() {
        final GenericController controller = new MyAnnotatedClass();
        controller.initService(MyAnnotatedClass.class);
        final Object service = controller.getService(OpaConstants.GENERIC_CONTROLLER);
        Assert.assertNotNull(service);
    }

    @UsingOpenPortalAdapterService(serviceNames = { "service" })
    private class MockAnnotated extends MyAnnotatedClass {

    }

    @Test(expectedExceptions = NoSuchBeanDefinitionException.class)
    public void initWithUnknownService() {
        final GenericController controller = new MockAnnotated();
        controller.initService(MockAnnotated.class);
        final Object service = controller.getService(OpaConstants.GENERIC_CONTROLLER);
        Assert.assertNotNull(service);
    }
}
