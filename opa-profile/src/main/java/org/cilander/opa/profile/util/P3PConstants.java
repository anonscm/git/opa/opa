/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.profile.util;

/**
 * UtilClass for the user attributes.
 * 
 * @author <a href="mailto:julien@jboss.org">Julien Viet</a>
 * @author <a href="mailto:chris.laprun@jboss.com">Chris Laprun</a>
 * @version $Revision: 8784 $
 */
public final class P3PConstants {

    /*
     * User information attribute names (PLT.D in the portlet spec) that are defined in P3P spec.
     */

    /**
     * see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BDATE = "user.bdate";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String INFO_USER_GENDER = "user.gender";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String INFO_USER_EMPLOYER = "user.employer";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String INFO_USER_DEPARTMENT = "user.department";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String INFO_USER_JOB_TITLE = "user.jobtitle";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String INFO_USER_NAME_PREFIX = "user.name.prefix";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String INFO_USER_NAME_GIVEN = "user.name.given";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String INFO_USER_NAME_FAMILY = "user.name.family";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String INFO_USER_NAME_MIDDLE = "user.name.middle";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String INFO_USER_NAME_SUFFIX = "user.name.suffix";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String INFO_USER_NAME_NICKNAME = "user.name.nickName";

    // Postal
    /**
     * see the p3p spec of the w3c.
     */
    public static final String POSTAL_NAME = "postal.name";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String POSTAL_STREET = "postal.street";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String POSTAL_CITY = "postal.city";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String POSTAL_STATEPROV = "postal.stateprov";

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String POSTAL_POSTALCODE = "postal.postalcode";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String POSTAL_COUNTRY = "postal.country";

    /**
     * see the p3p spec of the w3c.
     */
    public static final String POSTAL_ORGANIZATION = "postal.organization";

    // Telecom

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String TELECOM_TELEPHONE = "telecom.telephone.";

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String TELECOM_FAX = "telecom.fax.";

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String TELECOM_MOBILE = "telecom.mobile.";

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String TELECOM_PAGER = "telecom.pager.";

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INTCODE = "intcode";

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String LOCCODE = "loccode";

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String NUMBER = "number";

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String EXT = "ext";

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String COMMENT = "comment";

    // Online

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String ONLINE_URI = "online.uri";

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String ONLINE_EMAIL = "online.email";

    // User home
    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_PREFIX = "user.home-info.";

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_POSTAL_NAME = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.POSTAL_NAME;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_POSTAL_STREET = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.POSTAL_STREET;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_POSTAL_CITY = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.POSTAL_CITY;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_POSTAL_STATEPROV = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.POSTAL_STATEPROV;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_POSTAL_POSTALCODE = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.POSTAL_POSTALCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_POSTAL_COUNTRY = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.POSTAL_COUNTRY;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_POSTAL_ORGANIZATION = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.POSTAL_ORGANIZATION;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_TELEPHONE_INTCODE = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_TELEPHONE
            + P3PConstants.INTCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_TELEPHONE_LOCCODE = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_TELEPHONE
            + P3PConstants.LOCCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_TELEPHONE_NUMBER = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_TELEPHONE
            + P3PConstants.NUMBER;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_TELEPHONE_EXT = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_TELEPHONE
            + P3PConstants.EXT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_TELEPHONE_COMMENT = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_TELEPHONE
            + P3PConstants.COMMENT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_FAX_INTCODE = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_FAX + P3PConstants.INTCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_FAX_LOCCODE = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_FAX + P3PConstants.LOCCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_FAX_NUMBER = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_FAX + P3PConstants.NUMBER;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_FAX_EXT = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_FAX + P3PConstants.EXT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_FAX_COMMENT = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_FAX + P3PConstants.COMMENT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_MOBILE_INTCODE = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_MOBILE
            + P3PConstants.INTCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_MOBILE_LOCCODE = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_MOBILE
            + P3PConstants.LOCCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_MOBILE_NUMBER = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_MOBILE
            + P3PConstants.NUMBER;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_MOBILE_EXT = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_MOBILE + P3PConstants.EXT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_MOBILE_COMMENT = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_MOBILE
            + P3PConstants.COMMENT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_PAGER_INTCODE = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_PAGER
            + P3PConstants.INTCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_PAGER_LOCCODE = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_PAGER
            + P3PConstants.LOCCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_PAGER_NUMBER = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_PAGER + P3PConstants.NUMBER;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_PAGER_EXT = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_PAGER + P3PConstants.EXT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_TELECOM_PAGER_COMMENT = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.TELECOM_PAGER
            + P3PConstants.COMMENT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_ONLINE_EMAIL = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.ONLINE_EMAIL;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_HOME_INFO_ONLINE_URI = P3PConstants.INFO_USER_HOME_PREFIX + P3PConstants.ONLINE_URI;

    // User Business
    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_PREFIX = "user.business-info.";

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_POSTAL_NAME = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.POSTAL_NAME;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_POSTAL_STREET = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.POSTAL_STREET;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_POSTAL_CITY = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.POSTAL_CITY;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_POSTAL_STATEPROV = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.POSTAL_STATEPROV;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_POSTAL_POSTALCODE = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.POSTAL_POSTALCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_POSTAL_COUNTRY = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.POSTAL_COUNTRY;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_POSTAL_ORGANIZATION = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.POSTAL_ORGANIZATION;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_TELEPHONE_INTCODE = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_TELEPHONE
            + P3PConstants.INTCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_TELEPHONE_LOCCODE = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_TELEPHONE
            + P3PConstants.LOCCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_TELEPHONE_NUMBER = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_TELEPHONE
            + P3PConstants.NUMBER;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_TELEPHONE_EXT = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_TELEPHONE
            + P3PConstants.EXT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_TELEPHONE_COMMENT = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_TELEPHONE
            + P3PConstants.COMMENT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_FAX_INTCODE = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_FAX
            + P3PConstants.INTCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_FAX_LOCCODE = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_FAX
            + P3PConstants.LOCCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_FAX_NUMBER = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_FAX
            + P3PConstants.NUMBER;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_FAX_EXT = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_FAX + P3PConstants.EXT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_FAX_COMMENT = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_FAX
            + P3PConstants.COMMENT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_MOBILE_INTCODE = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_MOBILE
            + P3PConstants.INTCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_MOBILE_LOCCODE = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_MOBILE
            + P3PConstants.LOCCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_MOBILE_NUMBER = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_MOBILE
            + P3PConstants.NUMBER;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_MOBILE_EXT = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_MOBILE
            + P3PConstants.EXT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_MOBILE_COMMENT = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_MOBILE
            + P3PConstants.COMMENT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_PAGER_INTCODE = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_PAGER
            + P3PConstants.INTCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_PAGER_LOCCODE = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_PAGER
            + P3PConstants.LOCCODE;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_PAGER_NUMBER = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_PAGER
            + P3PConstants.NUMBER;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_PAGER_EXT = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_PAGER
            + P3PConstants.EXT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_TELECOM_PAGER_COMMENT = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.TELECOM_PAGER
            + P3PConstants.COMMENT;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_ONLINE_EMAIL = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.ONLINE_EMAIL;

    /**
     * for details see the p3p spec of the w3c.
     */
    public static final String INFO_USER_BUSINESS_INFO_ONLINE_URI = P3PConstants.INFO_USER_BUSINESS_PREFIX + P3PConstants.ONLINE_URI;

    /**
     * Constructor for P3PConstants.
     */
    public P3PConstants() {
        // nothing to do
    }
}
