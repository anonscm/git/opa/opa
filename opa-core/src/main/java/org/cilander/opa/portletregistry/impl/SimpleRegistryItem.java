package org.cilander.opa.portletregistry.impl;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * This container class is used to store the registry data for the simple registry.
 *
 * It just stores the given data. The portletId and the servicename are used as
 * identification criteria for uniqueness in equals and hashcode.
 * @author pcorne
 */
class SimpleRegistryItem {

  private String portletId;
  private String baseUrl;
  private String serviceName;

  public SimpleRegistryItem(String pPortletId, String pBaseUrl, String pServiceName) {
    super();
    portletId = pPortletId;
    baseUrl = pBaseUrl;
    serviceName = pServiceName;
  }

  /**
   * {@inheritDoc }
   * @param obj
   * @return
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == null || !(obj instanceof SimpleRegistryItem)) {
      return false;
    }
    SimpleRegistryItem other = (SimpleRegistryItem) obj;
    EqualsBuilder builder = new EqualsBuilder();
    builder.append(this.getPortletId(), other.getPortletId());
    builder.append(this.getServiceName(), other.getServiceName());
    return builder.isEquals();
  }

  /**
   * {@inheritDoc }
   * @return
   */
  @Override
  public int hashCode() {
    HashCodeBuilder builder = new HashCodeBuilder();
    builder.append(this.getPortletId());
    builder.append(this.getServiceName());
    return builder.toHashCode();
  }

  /**
   * @return the portletId
   */
  public String getPortletId() {
    return portletId;
  }

  /**
   * @param portletId
   *            the portletId to set
   */
  public void setPortletId(String portletId) {
    this.portletId = portletId;
  }

  /**
   * @return the baseUrl
   */
  public String getBaseUrl() {
    return baseUrl;
  }

  /**
   * @param baseUrl
   *            the baseUrl to set
   */
  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  /**
   * @return the serviceName
   */
  public String getServiceName() {
    return serviceName;
  }

  /**
   * @param serviceName
   *            the serviceName to set
   */
  public void setServiceName(String serviceName) {
    this.serviceName = serviceName;
  }
}
