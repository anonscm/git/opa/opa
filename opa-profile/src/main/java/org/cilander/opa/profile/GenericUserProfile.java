/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.profile;

import javax.portlet.PortletRequest;

/**
 * Interface that should be extended by all specialized modules.
 * 
 * @author oschmitz
 * @version $Revision$ $Date$
 */
public interface GenericUserProfile {

    /**
     * Convenience method for getting the first name of the user.
     * 
     * @param request
     * @return first name of the user is returned if set else null
     */
    String getFirstName(PortletRequest request);

    /**
     * Convenience method for getting the last name of the user.
     * 
     * @param request
     * @return last name of the user is returned if set else null
     */
    String getLastName(PortletRequest request);

    /**
     * Convenience method for getting the email address of the user.
     * 
     * @param request
     * @return email address of the user is returned if set else null
     */
    String getEmail(PortletRequest request);

    /**
     * Convenience method for getting the login name of the user.
     * 
     * @param request
     * @return login name of the user is returned if set else null
     */
    String getLoginName(PortletRequest request);

    /**
     * Convenience method for getting the ID of the user.
     * 
     * @param request
     * @return ID of the user as string
     */
    String getUserId(PortletRequest request);

    /**
     * Create user from stored user attributes in {@PortletRequest}.
     * 
     * @param request
     * @return {@link User}
     */
    User getGenericUser(final PortletRequest request);

}
