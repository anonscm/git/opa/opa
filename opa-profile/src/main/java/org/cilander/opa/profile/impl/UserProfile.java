/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
/**
 * Created: 11.12.2008<br>
 * Creator: Administrator
 */
package org.cilander.opa.profile.impl;

import org.cilander.opa.profile.User;

import java.util.Date;

/**
 * OPA User Profile.
 * 
 * @author Mohamed Ghazouani
 */
public class UserProfile implements User {

    private String email;
    private String firstName;
    private String lastName;
    private String loginName;
    private String userId;
    private Gender gender;
    private Date birthDate;

    public String getEmail() {
        return this.email == null ? "" : this.email;
    }

    public String getFirstName() {
        return this.firstName == null ? "" : this.firstName;
    }

    public String getLastName() {
        return this.lastName == null ? "" : this.lastName;
    }

    public String getLoginName() {
        return this.loginName == null ? "" : this.loginName;
    }

    public void setEmail(final String value) {
        this.email = value;
    }

    public void setFirstName(final String value) {
        this.firstName = value;
    }

    public void setLastName(final String value) {
        this.lastName = value;

    }

    public void setLoginName(final String value) {
        this.loginName = value;

    }

    public void setUserId(final String value) {
        this.userId = value;

    }

    public String getUserId() {
        return this.userId == null ? "" : this.userId;
    }

    public Gender getGender() {
        return this.gender;
    }

    public void setGender(final Gender gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(final Date birthDate) {
        this.birthDate = birthDate;
    }

}
