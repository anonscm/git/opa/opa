/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.profile;

import org.cilander.opa.profile.impl.UserProfile;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Date;

public class UserProfileTest {

    UserProfile user;

    @BeforeTest
    public void setUp() {
        this.user = new UserProfile();
        this.user.setBirthDate(new Date());
        this.user.setEmail("test@liferay.com");
        this.user.setFirstName("tester");
        this.user.setGender(User.Gender.MALE);
        this.user.setLastName("tester");
        this.user.setLoginName("admin");
        this.user.setUserId("1");
    }

    @Test
    public final void testGetEmail() {
        Assert.assertNotNull(this.user.getEmail());
        this.user.setEmail(null);
        Assert.assertNotNull(this.user.getEmail());
    }

    @Test
    public final void testGetFirstName() {
        Assert.assertNotNull(this.user.getFirstName());
        this.user.setFirstName(null);
        Assert.assertNotNull(this.user.getFirstName());
    }

    @Test
    public final void testGetLastName() {
        Assert.assertNotNull(this.user.getLastName());
        this.user.setLastName(null);
        Assert.assertNotNull(this.user.getLastName());
    }

    @Test
    public final void testGetLoginName() {
        Assert.assertNotNull(this.user.getLoginName());
        this.user.setLoginName(null);
        Assert.assertNotNull(this.user.getLoginName());
    }

    @Test
    public final void testGetUserId() {
        Assert.assertNotNull(this.user.getUserId());
        this.user.setUserId(null);
        Assert.assertNotNull(this.user.getUserId());
    }

    @Test
    public final void testGetGender() {
        Assert.assertEquals(this.user.getGender(), User.Gender.MALE);
    }

    @Test
    public final void testGetBirthDate() {
        Assert.assertNotNull(this.user.getBirthDate());
    }

}
