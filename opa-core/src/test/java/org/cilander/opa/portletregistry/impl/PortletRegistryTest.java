package org.cilander.opa.portletregistry.impl;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.cilander.opa.portletregistry.PortletRegistry;
import org.cilander.opa.portletregistry.PortletRegistryFactory;

public class PortletRegistryTest extends TestCase {

	public void testRegisterPortlet() {
		PortletRegistry registry = PortletRegistryFactory.getInstance().getPortletRegistry();
		final String baseUrl = "baseUrl";
		final String serviceName = "serviceName";
		final String portletId = "portletId";
		registry.registerPortlet(portletId, baseUrl, serviceName);
		assertNotNull(registry.getEncodedUrl(serviceName));
		assertEquals(baseUrl, registry.getEncodedUrl(serviceName));

	}

	public void testUnRegisterPortlet() {
		PortletRegistry registry = PortletRegistryFactory.getInstance().getPortletRegistry();
		final String baseUrl = "baseUrl";
		final String serviceName = "serviceName";
		final String portletId = "portletId";

		registry.registerPortlet(portletId, baseUrl, serviceName);
		assertNotNull(registry.getEncodedUrl(serviceName));

		registry.unregisterPortlet(portletId);
		assertNull(registry.getEncodedUrl(serviceName));
	}

	public void testEncodedUrlSimple() {
		PortletRegistry registry = PortletRegistryFactory.getInstance().getPortletRegistry();
		final String baseUrl = "baseUrl";
		final String serviceName = "serviceName";
		final String portletId = "portletId";

		registry.registerPortlet(portletId, baseUrl, serviceName);
		assertNotNull(registry.getEncodedUrl(serviceName));

		assertEquals(baseUrl, registry.getEncodedUrl(serviceName));

	}

	public void testEncodedUrlParametersWithQMark() {
		PortletRegistry registry = PortletRegistryFactory.getInstance().getPortletRegistry();
		final String baseUrl = "http://test?a=2";
		final String serviceName = "serviceName";
		final String portletId = "portletId";

		registry.registerPortlet(portletId, baseUrl, serviceName);

		assertEquals(baseUrl, registry.getEncodedUrl(serviceName));

	}

	public void testEncodedUrlParametersWithoutQMark() {
		PortletRegistry registry = PortletRegistryFactory.getInstance().getPortletRegistry();
		final String baseUrl = "http://test?a=2";
		final String serviceName = "serviceName";
		final String portletId = "portletId";

		registry.registerPortlet(portletId, baseUrl, serviceName);
		assertEquals(baseUrl, registry.getEncodedUrl(serviceName));

		final Map<String, String> params = new HashMap<String, String>();
		params.put("b", "3");
		params.put("c", "4 5");
		final String resultingUrl = registry.getEncodedUrl(serviceName, params);
		assertTrue(resultingUrl.contains("&b=3"));
		assertTrue(resultingUrl.contains("&c=4+5"));
	}
}
