/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.profile;

import org.cilander.opa.profile.impl.GenericUserProfileImpl;
import org.cilander.opa.profile.util.P3PConstants;
import org.springframework.mock.web.portlet.MockRenderRequest;
import org.springframework.mock.web.portlet.MockRenderResponse;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import javax.portlet.GenericPortlet;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

@Test
public class GenericUserProfileTest {

    private GenericUserProfileImpl profile = null;
    private Map<String, String> userMap = new HashMap<String, String>();
    private final MyMockPortlet portlet = new MyMockPortlet();

    private class MyMockPortlet extends GenericPortlet {

        public PortletRequest getPortletRequest() {
            final MockRenderRequest request = new MockRenderRequest();
            request.setAttribute(PortletRequest.USER_INFO, userMap);
            return request;
        }

        public PortletResponse getPortletResponse() {
            return new MockRenderResponse();
        }

    }

    private class MockPortlet extends GenericPortlet {

        public PortletRequest getPortletRequest() {
            final MockRenderRequest request = new MockRenderRequest();
            return request;
        }

        public PortletResponse getPortletResponse() {
            return new MockRenderResponse();
        }

    }

    private void createUserMap() {
        userMap.put(P3PConstants.INFO_USER_NAME_NICKNAME, "Test");
        userMap.put(P3PConstants.INFO_USER_BUSINESS_INFO_ONLINE_EMAIL, "Test@liferay.com");
        userMap.put(P3PConstants.INFO_USER_NAME_GIVEN, "Joe");
        userMap.put(P3PConstants.INFO_USER_NAME_FAMILY, "Bloggs");
        profile = new GenericUserProfileImpl();
    }

    @BeforeTest
    public void setUp() {
        createUserMap();
    }

    @AfterTest
    public void tearDown() {
        userMap = null;
        profile = null;
    }

    @Test
    public void constructorWithNull() {
        new GenericUserProfileImpl();
    }

    @Test
    public void getUserEmail() {
        final String email = profile.getEmail(portlet.getPortletRequest());
        Assert.assertNotNull(email);
        Assert.assertEquals(email, "Test@liferay.com");
    }

    @Test
    public void getUserName() {
        final String loginName = profile.getLoginName(portlet.getPortletRequest());
        Assert.assertNotNull(loginName);
        Assert.assertEquals(loginName, "Test");
    }

    @Test
    public void getFirstName() {
        final String firstName = profile.getFirstName(portlet.getPortletRequest());
        Assert.assertNotNull(firstName);
        Assert.assertEquals(firstName, "Joe");
    }

    @Test
    public void getLastName() {
        final String lastName = profile.getLastName(portlet.getPortletRequest());
        Assert.assertNotNull(lastName);
        Assert.assertEquals(lastName, "Bloggs");
    }

    @Test
    public void getUnAuthorizedValue() {
        final String email = profile.getEmail(new MockPortlet().getPortletRequest());
        Assert.assertNotNull(email);
        Assert.assertEquals(email, "");
    }

    @Test
    public void getUserId() {
        final String id = profile.getUserId(portlet.getPortletRequest());
        Assert.assertNull(id);
    }

    @Test
    public void getValueNotBeingSet() {
        final Map<String, String> newUserMap = new HashMap<String, String>();
        newUserMap.put(P3PConstants.INFO_USER_NAME_NICKNAME, "Test");
        newUserMap.put(P3PConstants.INFO_USER_BUSINESS_INFO_ONLINE_EMAIL, null);
        newUserMap.put(P3PConstants.INFO_USER_NAME_GIVEN, "Joe");
        newUserMap.put(P3PConstants.INFO_USER_NAME_FAMILY, "Bloggs");
        final MockRenderRequest request = new MockRenderRequest();
        request.setAttribute(PortletRequest.USER_INFO, newUserMap);
        profile = new GenericUserProfileImpl();
        final String email = profile.getEmail(request);
        Assert.assertEquals(email, "");
    }

    @Test
    public void getPortletRequestNotSet() {
        profile = new GenericUserProfileImpl();
        final String email = profile.getEmail(null);
        Assert.assertEquals(email, "");
    }

    @Test
    public void getUserFromPortletRequest() {
        final User user = profile.getGenericUser(portlet.getPortletRequest());
        Assert.assertNotNull(user);
    }

}
