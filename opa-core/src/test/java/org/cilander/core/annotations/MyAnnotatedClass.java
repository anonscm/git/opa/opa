/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.core.annotations;

import org.cilander.opa.core.annotation.Secured;
import org.cilander.opa.core.annotation.UsingOpenPortalAdapterService;
import org.cilander.opa.core.constants.OpaConstants;
import org.cilander.opa.core.controller.GenericController;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Secured(name = "MyAnnotatedClass", value = "toBeTested")
@UsingOpenPortalAdapterService(serviceNames = { OpaConstants.GENERIC_CONTROLLER })
public class MyAnnotatedClass extends GenericController {

    public MyAnnotatedClass() {
        setApplicationContext(new ClassPathXmlApplicationContext(new String[] { "org.cilander.opa.context.xml" }));
    }

}
