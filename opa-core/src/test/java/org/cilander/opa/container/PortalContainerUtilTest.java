/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.container;

import org.cilander.opa.util.PortalContainerUtil;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PortalContainerUtilTest {

    @Test
    public void containerNotIdentifiedTest() {
        Assert.assertFalse(PortalContainerUtil.canLoadClass("com.liferay.portal.kernel.configuration.ConfigurationFactoryUtil"));
        Assert.assertFalse(PortalContainerUtil.canLoadClass("org.jboss.portal.core.identity.services.impl.IdentityUserManagementServiceImpl"));
    }

    @Test
    public void canLoadClassTest() {
        Assert.assertTrue(PortalContainerUtil.canLoadClass("java.lang.String"));
    }

    @Test
    public void liferayContainerTest() {
        Assert.assertFalse(PortalContainerUtil.isLiferayPortalContainer());
    }

    @Test
    public void jbossContainerTest() {
        Assert.assertFalse(PortalContainerUtil.isJbossPortalContainer());
    }

}
