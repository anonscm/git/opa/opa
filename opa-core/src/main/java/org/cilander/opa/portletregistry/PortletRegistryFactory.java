package org.cilander.opa.portletregistry;

import org.cilander.opa.portletregistry.impl.SimplePortletRegistryImpl;

/**
 * This class can be used to obtain a singleton instance of a PortletRegistry.
 * (enforced by static instance)
 * 
 * @author S.Tinder, tarent GmbH
 */
public class PortletRegistryFactory {

	/** The registry. */
	private final PortletRegistry registry;

	/** The instance. */
	private static PortletRegistryFactory instance = new PortletRegistryFactory();

	/**
	 * Gets the single instance of PortletRegistryFactory.
	 * 
	 * @return single instance of PortletRegistryFactory
	 */
	public static PortletRegistryFactory getInstance() {
		return instance;
	}

	/**
	 * Instantiates a new portlet registry factory.
	 */
	private PortletRegistryFactory() {
		registry = new SimplePortletRegistryImpl();
	}

	/**
	 * Gets the portlet registry.
	 * 
	 * @return the portlet registry
	 */
	public PortletRegistry getPortletRegistry() {
		return registry;
	}
}
