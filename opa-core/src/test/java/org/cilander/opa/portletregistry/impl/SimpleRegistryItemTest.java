package org.cilander.opa.portletregistry.impl;

import junit.framework.TestCase;

/**
 *
 * @author pcorne
 */
public class SimpleRegistryItemTest extends TestCase {
    
  /**
   * Test of equals method, of class SimpleRegistryItem.
   */
  public void testEquals() {
    System.out.println("equals");
    SimpleRegistryItem obj = new SimpleRegistryItem("1", "2", "3");
    SimpleRegistryItem instance = new SimpleRegistryItem("1", "4", "3");
    assertEquals(true, instance.equals(obj));
    obj = new SimpleRegistryItem("1", "2", "4");
    assertEquals(false, instance.equals(obj));
  }

  /**
   * Test of hashCode method, of class SimpleRegistryItem.
   */
  public void testHashCode() {
    System.out.println("hashCode");
    SimpleRegistryItem obj = new SimpleRegistryItem("1", "2", "3");
    SimpleRegistryItem instance = new SimpleRegistryItem("1", "4", "3");
    assertEquals(obj.hashCode(), instance.hashCode());
    obj = new SimpleRegistryItem("1", "2", "4");
    assertFalse(obj.hashCode()==instance.hashCode());
  }


  /**
   * Test of set/getPortletId method, of class SimpleRegistryItem.
   */
  public void testSetPortletId() {
    System.out.println("setPortletId");
    String expResult = "1-1-1";
    SimpleRegistryItem instance = new SimpleRegistryItem(expResult, "2", "3");
    String result = instance.getPortletId();
    assertEquals(expResult, result);
    expResult=expResult+"-1";
    instance.setPortletId(expResult);
    assertEquals(expResult, instance.getPortletId());
  }

  /**
   * Test of set/getBaseUrl method, of class SimpleRegistryItem.
   */
  public void testSetBaseUrl() {
    System.out.println("getBaseUrl");
    String expResult = "1-1-1";
    SimpleRegistryItem instance = new SimpleRegistryItem("1",expResult, "3");
    String result = instance.getBaseUrl();
    assertEquals(expResult, result);
    expResult=expResult+"-1";
    instance.setBaseUrl(expResult);
    assertEquals(expResult, instance.getBaseUrl());
  }

  /**
   * Test of set/getServiceName method, of class SimpleRegistryItem.
   */
  public void testSetServiceName() {
    System.out.println("getServiceName");
    String expResult = "1-1-1";
    SimpleRegistryItem instance = new SimpleRegistryItem("1","2",expResult);
    String result = instance.getServiceName();
    assertEquals(expResult, result);
    expResult=expResult+"-1";
    instance.setServiceName(expResult);
    assertEquals(expResult, instance.getServiceName());
  }

}
