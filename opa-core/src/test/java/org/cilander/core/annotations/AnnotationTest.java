/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.core.annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import org.cilander.opa.core.annotation.Secured;
import org.cilander.opa.core.annotation.UsingOpenPortalAdapterService;
import org.cilander.opa.core.controller.GenericController;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Testcase for the Annotations.
 * 
 * @author Olaf
 * @author Mohamed
 */
public class AnnotationTest {

    @Test
    public void getAnnotatedValue() {
        boolean failed = false;
        final Class aClass = MyAnnotatedClass.class;
        final Annotation[] annotations = aClass.getAnnotations();
        for (final Annotation annotation : annotations) {
            if (annotation instanceof Secured) {
                final Secured myAnnotation = (Secured) annotation;
                Assert.assertEquals(myAnnotation.name(), "MyAnnotatedClass");
                Assert.assertEquals(myAnnotation.value(), "toBeTested");
            }
        }

        final Field[] declaredFields = aClass.getDeclaredFields();
        for (final Field field : declaredFields) {
            final Annotation[] methodAnnotations = field.getAnnotations();
            for (Annotation annotation : methodAnnotations) {
                if (annotation instanceof UsingOpenPortalAdapterService) {
                    try {
                        final UsingOpenPortalAdapterService myAnnotation = (UsingOpenPortalAdapterService) annotation;
                        Assert.assertEquals(myAnnotation.serviceNames()[0], "service");

                        final Object newInstance = aClass.newInstance();

                        Assert.assertFalse(((GenericController) newInstance).getApplicationContext().containsBean(myAnnotation.serviceNames()[0]));

                        final Object invoked = field.get(newInstance);
                        final String result = (String) invoked;
                        Assert.assertEquals(result, "service");
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                        failed = true;
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                        failed = true;
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                        failed = true;
                    }
                }
            }
            Assert.assertFalse(failed);
        }

    }

}
