/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
/**
 * 
 */
package org.cilander.opa.profile.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cilander.opa.profile.GenericUserProfile;
import org.cilander.opa.profile.User;
import org.cilander.opa.profile.util.P3PConstants;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import javax.portlet.PortletRequest;

/**
 * Implementation of the GenericUserProfile interface.
 * 
 * @author oschmitz
 */
public class GenericUserProfileImpl implements GenericUserProfile {

    /**
     * Logger for this Class.
     */
    private static final transient Log LOGGER = LogFactory.getLog(GenericUserProfileImpl.class);

    /**
     * Variable for the user attributes.
     */
    private Map<String, String> userMap = new HashMap<String, String>();

    /**
     * {@inheritDoc}
     * 
     * @see org.cilander.opa.profile.GenericUserProfile#getEmail(PortletRequest)
     */
    public final String getEmail(final PortletRequest request) {
        return getValue(request, P3PConstants.INFO_USER_BUSINESS_INFO_ONLINE_EMAIL);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.cilander.opa.profile.GenericUserProfile#getFirstName(PortletRequest)
     */
    public final String getFirstName(final PortletRequest request) {
        return getValue(request, P3PConstants.INFO_USER_NAME_GIVEN);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.cilander.opa.profile.GenericUserProfile#getLastName(PortletRequest)
     */
    public final String getLastName(final PortletRequest request) {
        return getValue(request, P3PConstants.INFO_USER_NAME_FAMILY);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.cilander.opa.profile.GenericUserProfile#getLoginName(PortletRequest)
     */
    public final String getLoginName(final PortletRequest request) {
        return getValue(request, P3PConstants.INFO_USER_NAME_NICKNAME);
    }

    /**
     * Method for getting the value of the userMap.
     * 
     * @param keyValue
     * @return
     */
    @SuppressWarnings("unchecked")
    private String getValue(final PortletRequest request, final String keyValue) {
        final PortletRequest portletRequest = request;
        if (portletRequest == null) {
            GenericUserProfileImpl.LOGGER.warn("PortletRequest was null !");
            return "";
        }
        userMap = (Map<String, String>) portletRequest.getAttribute(PortletRequest.USER_INFO);
        if (userMap == null) {
            GenericUserProfileImpl.LOGGER.warn("Usermap was null. Seems to be a guest user");
            return "";
        }
        final String value = userMap.get(keyValue);
        if (value == null) {
            GenericUserProfileImpl.LOGGER.warn(MessageFormat.format("{0} was null please check if it is defined in the portlet.xml", keyValue));
            return "";
        }
        return value;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.cilander.opa.profile.GenericUserProfile#getUserId(PortletRequest)
     */
    public String getUserId(final PortletRequest request) {
        // should be overwritten
        return null;
    }

    public User getGenericUser(final PortletRequest request) {
        final UserProfile user = new UserProfile();
        user.setFirstName(getFirstName(request));
        user.setLastName(getLastName(request));
        user.setEmail(getEmail(request));
        user.setLoginName(getLoginName(request));
        return user;
    }
}
