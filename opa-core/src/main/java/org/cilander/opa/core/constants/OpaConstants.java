/**
 * This file is part of the Open Portal Adapter
 *
 * Copyright (c) 2008 kingmedia websolutions GmbH (http://www.kingmedia.de)
 *
 * The open portal adapter is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * The open portal adapter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about kingmedia websolutions GmbH, please see the
 * company website: http://www.kingmedia.de
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with the cilander business suite; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package org.cilander.opa.core.constants;

/**
 * UtilClass that contains the Servicenames in a later step this should be done via Properties.
 * 
 * @author oschmitz
 */
public final class OpaConstants {

    /**
     * given name of the userService.
     */
    public static final String USER_SERVICE = "userService";

    /**
     * given name of the securityService.
     */
    public static final String SECURITY_SERVICE = "securityService";// System.getProperty("sercuritService",
    // "sercuritService");

    /**
     * given name of the genericController.
     */
    public static final String GENERIC_CONTROLLER = "genericController";// System.getProperty(, "genericController");

    /**
     * given name for userServiceUtil
     */

    public static final String USER_SERVICE_UTIL_NAME = "org.cilander.opa.service.userServiceUtil";
    /**
     * class name to check for jboss portal
     */
    public static final String JBOSS_CONTAINER_DETECTION_CLASS_NAME = "org.jboss.portal.core.identity.services.impl.IdentityUserManagementServiceImpl";

    /**
     * class name to check for liferay portal
     */

    public static final String LIFERAY_CONTAINER_DETECTION_CLASS_NAME = "com.liferay.portal.service.UserLocalServiceUtil";

    /**
     * Get the value of uSER_SERVICE.
     * 
     * @return the uSER_SERVICE
     */
    public static String getUserService() {
        return OpaConstants.USER_SERVICE;
    }

    /**
     * Get the value of sECURITY_SERVICE.
     * 
     * @return the sECURITY_SERVICE
     */
    public static String getSecurityService() {
        return OpaConstants.SECURITY_SERVICE;
    }

    /**
     * Get the value of gENERIC_CONTROLLER.
     * 
     * @return the gENERIC_CONTROLLER
     */
    public static String getGenericController() {
        return OpaConstants.GENERIC_CONTROLLER;
    }

    private OpaConstants() {

    }

    private static OpaConstants instance;

    public static synchronized OpaConstants getInstance() {
        if (OpaConstants.instance == null) {
            OpaConstants.instance = new OpaConstants();
        }
        return OpaConstants.instance;
    }

}
